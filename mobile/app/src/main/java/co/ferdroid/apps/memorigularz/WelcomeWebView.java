package co.ferdroid.apps.memorigularz;


import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidsx.rateme.RateMeDialog;
import com.androidsx.rateme.RateMeDialogTimer;


public class WelcomeWebView extends AppCompatActivity {

    private WebView herokuapp_browser;

    private AlertDialog alerts;

    private TextView signal_wifi_message;
    private ImageView signal_wifi_icon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_PROGRESS);

        setContentView(R.layout.activity_welcome_web_view);


        signal_wifi_icon = (ImageView) findViewById(R.id.signal_wifi_off);
        signal_wifi_message = (TextView) findViewById(R.id.txt_signal_wifi_message);




        herokuapp_browser = (WebView) findViewById(R.id.herokuapp_browser);

        if (isNetworkAvailable()) {

            herokuapp_browser.setVisibility(View.VISIBLE);
            signal_wifi_icon.setVisibility(View.GONE);
            signal_wifi_message.setVisibility(View.GONE);


            herokuapp_browser.getSettings().setJavaScriptEnabled(true);

            herokuapp_browser.setWebChromeClient(new WebChromeClient() {
                public void onProgressChanged(WebView view, int progress) {
                    // Activities and WebViews measure progress with different scales.
                    // The progress meter will automatically disappear when we reach 100%
                    WelcomeWebView.this.setProgress(progress * 1000);
                }
            });
            herokuapp_browser.setWebViewClient(new WebViewClient() {
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    Toast.makeText(WelcomeWebView.this, "Oh no! " + description, Toast.LENGTH_SHORT).show();
                }
            });

            herokuapp_browser.getSettings().setBuiltInZoomControls(false);

            herokuapp_browser.loadUrl(getString(R.string.webUrlApp));

        } else {
            herokuapp_browser.setVisibility(View.INVISIBLE);

            Log.d("## network state", "there is no internet");
            signal_wifi_icon.setVisibility(View.VISIBLE);
            signal_wifi_message.setVisibility(View.VISIBLE);

            Toast.makeText(this, getString(R.string.toast_no_wifi_message), Toast.LENGTH_LONG).show();
        }


    }


    @Override
    public void onBackPressed() {


        if (isNetworkAvailable()) {


            RateMeDialogTimer.onStart(WelcomeWebView.this);
            if (RateMeDialogTimer.wasRated(WelcomeWebView.this)) {
                // do nothing if it was rated

                alerts = new AlertDialog.Builder(WelcomeWebView.this)
                        .setTitle(getString(R.string.askForExitApp_Title))
                        .setMessage(getString(R.string.askForExitApp_Message))
                        .setPositiveButton(getString(R.string.alert_yes), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                herokuapp_browser.clearCache(true);
                                herokuapp_browser.clearHistory();
                                herokuapp_browser.destroy();

                                CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(WelcomeWebView.this);
                                CookieManager cookieManager = CookieManager.getInstance();
                                cookieManager.removeAllCookie();


                                finish();


                            }
                        })
                        .setNegativeButton(getString(R.string.alert_no), null)
                        .setIcon(R.mipmap.ic_launcher)
                        .show();

            } else {
                if (RateMeDialogTimer.shouldShowRateDialog(WelcomeWebView.this, 0, 1)) {
                    new RateMeDialog.Builder(getPackageName(), getString(R.string.app_name))
                            .setBodyBackgroundColor(ContextCompat.getColor(WelcomeWebView.this, R.color.colorWhite))
                            .setBodyTextColor(ContextCompat.getColor(WelcomeWebView.this, R.color.colorBlack))
                            .setHeaderBackgroundColor(ContextCompat.getColor(WelcomeWebView.this, R.color.colorPrimaryDark))
                            .setHeaderTextColor(ContextCompat.getColor(WelcomeWebView.this, R.color.colorWhite))
                            .enableFeedbackByEmail("") // this one enable up only if the app was ported as a native, with facebook auth
                            .build()
                            .show(getFragmentManager(), "plain-dialog");
                }


            }

        } else {
            finish();
            herokuapp_browser.destroy();
        }

    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }




}
