# encoding: UTF-8
require 'sinatra'
#require 'data_mapper'
require 'find'
require 'json'
require './main'

set :root, File.dirname(__FILE__)
set :public_folder, settings.root + '/public'
set :views, settings.root + '/views'
set :classes, settings.root + '/classes'
set :models, settings.root + '/models'
set :erb, :format => :html5

set :environment, :development
set :raise_errors, true


use Rack::Session::Cookie, :key => 'sess_memorigularz',
                           :path => '/',
                           :expire_after => 14400, # In seconds
                           :secret => 'my_awesome_english_app'
#set :sessions, true
enable :sessions

set :logging, true

# autoloading controllers classes
Find.find(settings.classes) do |ctr|
	next if File.extname(ctr) != ".rb"
	puts "loading #{ctr}"
	load(ctr)
end

# autoloading models
#Find.find(settings.models) do |mdl|
#	next if File.extname(mdl) != ".rb"
#	puts "loading #{mdl}"
#	load(mdl)
#end

#Set-up the database using datamapper
#DataMapper.setup(:default, ENV['DATABASE_URL'] || "sqlite3://#{settings.root}/devel.db")


#Card.auto_migrate! # it is gonna destroy and re-create the table

#puts settings.root + '/verbs_list.txt'
# now I'm gonna open the file where I manage irregular verbs list

#File.open('verbs_list.txt', 'a+') do |file|
#	file.each_line do |line|
#		line_array = line.split(",")
#		Card.create(verb: line_array[0], 
#					type: line_array[1], 
#					gerund: line_array[2], 
#					image: (line_array[3].nil?) ? 'image_default.png' : line_array[3], 
#					pp: line_array[4], 
#					past: line_array[5])
#	end
#end

run Sinatra::Application
