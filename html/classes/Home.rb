class Home

	def initialize
	end

	def show_welcome
		'Welcome dear player!'
	end


	def set_cards
		
		cards_array = Array.new

		
		i = 0

		File.open( Dir.pwd + '/verbs_list.txt', 'a+') do |file|
			file.each_line do |line|
				line_array = line.split(",")
				cards_array.push({
								:id => i+1,
								:verb => line_array[0],
								:image => (line_array[3].nil?) ? 'image_default.png' : line_array[3],
								:gerund => line_array[2],
								:type => line_array[1],
								:pp => line_array[4],
								:past => line_array[5]
							 }.to_hash)
				i = i + 1
			end
		end
		
		return cards_array
	end



	def append_to_highscore(np, gt, m)

		File.open( Dir.pwd + '/highscores.txt', 'a+') do |file|
			file.puts "#{np},#{m},#{gt[0].to_str}:#{gt[1].to_str}"
		end

	end


	def get_highscore

		scores = Array.new

		if (File.exist?(Dir.pwd + '/highscores.txt'))
			i = 0
			File.open( Dir.pwd + '/highscores.txt', 'r+') do |file|
				file.each_line do |line|
					line_array = line.split(",")
					scores[i] = {
									:id => i+1,
									:player => line_array[0],
									:counts => line_array[1],
									:gtime => line_array[2]
								}.to_hash
					i = i + 1
				end
			end	
		end

		

		#puts scores.inspect

		# it is possible sorting like this but I think is better when 
		# this app is ported to native in android completely, using records 
		# from mysql or posgresql

		#scores = scores.sort { |a, b| a[:gtime] <=> b[:gtime] }
		#scores = scores.sort_by { |a| [a[:counts], a[:gtime]] }

		#puts scores.inspect

		return scores

	end

end