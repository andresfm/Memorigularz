# Memorigularz
A game about memorize whole the huge irregular or regular verbs list


### How to start webapp

Simply you can type 
```bash
bundle exec rackup config.ru
```
and automatically webserver starts to serve on port 9292

but if you want to change the port, just type the following:
```bash
bundle exec rackup config.ru -p $PORT
```
where $PORT is the desired port you want

or if you've rather can try typing 
```bash
rerun 'rackup config.ru -p $PORT'
```

